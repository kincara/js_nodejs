'use strict';

/* 
    JS Classes
	constructor
	"this" syntax
    inheritance
    
    ? -> chaining optional
*/

class Fruit { 

    constructor(taste) {
        this.taste = taste;
    } 

    getTaste() {
        return this.taste;
    }

}

class Banana extends Fruit {
    constructor(taste) {
        super(taste);
    } 
}

class Apple extends Fruit {
    constructor(taste, colour) { 
        super(taste);
        this.colour = colour;
    } 
}

let banana = new Banana('sweet');
let apple = new Apple('Yack!', 'red');
console.log(banana.getTaste());
console.log(apple.getTaste());

/************* Chaining optional *************/
console.log(banana.colour);
console.log(banana.colour?.shape); //explained bottom condition
let conditionChaining = 'colour' in banana ? banana.colour.shape : undefined; //rotund sau undefined