"use strict";

//let x = (params) => {};//arrow function

// function sayHello() {
//     console.log('Hello!');
// }

let sayHello = () => console.log('Hello');
sayHello();

// function calcSum(a, b) {
//     return a + b;
// }

let calcSum = (a, b) => a+b;
console.log(calcSum(4, 3));

// function checkNumberEven(a) {
//     return a%2 ? true : false;
// } 

let checkNumberEven = a => a%2 ? true : false;
console.log(checkNumberEven(5));