'use strict';

/* 
    Static variables
    Static methods
    Private variables
    Private Methods
*/

class Fruit {  

    static fruitsCount = 0;
    #weight = 0; 

    constructor(taste) {
        this.taste = taste;
        this.#setWeight(23);
        Fruit.fruitsCount++;
    }  

    #setWeight = (weight) => {
        this.#weight = weight;
    }

    getWeight() {
        return this.#weight;
    }

    getTaste() {
        return this.taste;
    }

    static getTotalFruits() {
        return Fruit.fruitsCount;
    }
    
}

class Banana extends Fruit {
    constructor(taste) {
        super(taste);
    } 
}

class Apple extends Fruit {
    constructor(taste, colour) { 
        super(taste);
        this.colour = colour;
    } 
} 

console.log(`Total fruits are ${Fruit.fruitsCount}`);
let banana = new Banana('sweet'); 
console.log(banana.getWeight());

//banana.setWeight(75);
//console.log(banana.getWeight());

let apple = new Apple('Yack!', 'red');   
console.log(`Total fruits are ${Fruit.getTotalFruits()}`);