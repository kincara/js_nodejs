"use strict";

let transmission = 'manual';//automatic

let car = {
    colour: 'red',
    model: 'tico',
    'body type': 'suv',
    [transmission]: true
}; 
 
console.log(car["body type"]); //get
console.log(car.manual); //get 
console.log(car.automatic); //get

car.weight = '1.2t';//set 

console.log(car);

console.log(car.colour); //get

delete car.model;

console.log(car);
console.log(car.model);//undefined 

console.log('model' in car);
console.log('colour' in car);

for( let key in car) {
    console.log(`Key is = ${key}`);
}

let obj1 = {
    3: 3,
    'maria': 'ioana',
    2: 2,
    1: 1
}

for( let keyNr in obj1) {
    console.log(`Key is = ${keyNr}`);
}